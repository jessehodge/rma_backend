#!/bin/bash
-e

backend_env/bin/activate
exec gunicorn --workers=2 wsgi:application
