from django.conf.urls import url, include
from rest_framework.routers import DefaultRouter
from v1.data.rma import urls as rma_urls
from v1.data.sales_cr_memo import urls as sales_cr_memo_urls
from v1.data.sales_invoice import urls as sales_invoice_urls
from v1.data.sales_line_item import urls as sales_line_item_urls
from v1.data.bom_component import urls as bom_urls

router = DefaultRouter()

urlpatterns = [
    url(r'^', include(bom_urls)),
    url(r'^', include(rma_urls)),
    url(r'^', include(sales_cr_memo_urls)),
    url(r'^', include(sales_invoice_urls)),
    url(r'^', include(sales_line_item_urls)),
]
