from rest_framework.routers import DefaultRouter
from django.conf.urls import url
from views import SalesInvoiceHeaderViewSet, SalesInvoiceLineViewSet, SalesInvoiceHeaderOrderNumberViewSet

router = DefaultRouter()
router.register(r'sales_invoice_header', SalesInvoiceHeaderViewSet, base_name='sales_invoice_header')

urlpatterns = [
    url('^sales_invoice_line/(?P<no_field>.+)/$', SalesInvoiceLineViewSet.as_view()),
    url('^sales_invoice_order_number/(?P<order_no>.+)/$', SalesInvoiceHeaderOrderNumberViewSet.as_view()),
]

urlpatterns += router.urls
