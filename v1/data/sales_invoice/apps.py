from __future__ import unicode_literals

from django.apps import AppConfig


class SalesInvoiceConfig(AppConfig):
    name = 'sales_invoice'
