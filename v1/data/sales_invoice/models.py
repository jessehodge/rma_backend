from __future__ import unicode_literals

from django.db import models

# Create your models here.


class KaemarkSalesInvoiceHeader(models.Model):
    no_field = models.CharField(db_column='No_', primary_key=True, max_length=20)
    sell_to_customer_no_field = models.CharField(db_column='Sell-to Customer No_', max_length=20)
    bill_to_customer_no_field = models.CharField(db_column='Bill-to Customer No_', max_length=20)
    # Delivery Information
    bill_to_name = models.CharField(db_column='Bill-to Name', max_length=50)
    bill_to_name_2 = models.CharField(db_column='Bill-to Name 2', max_length=50)
    bill_to_address = models.CharField(db_column='Bill-to Address', max_length=50)
    bill_to_address_2 = models.CharField(db_column='Bill-to Address 2', max_length=50)
    bill_to_city = models.CharField(db_column='Bill-to City', max_length=30)
    bill_to_contact = models.CharField(db_column='Bill-to Contact', max_length=50)
    your_reference = models.CharField(db_column='Your Reference', max_length=35)
    ship_to_code = models.CharField(db_column='Ship-to Code', max_length=10)
    # Delivery Information
    ship_to_name = models.CharField(db_column='Ship-to Name', max_length=50)
    ship_to_name_2 = models.CharField(db_column='Ship-to Name 2', max_length=50)
    ship_to_address = models.CharField(db_column='Ship-to Address', max_length=50)
    ship_to_address_2 = models.CharField(db_column='Ship-to Address 2', max_length=50)
    ship_to_city = models.CharField(db_column='Ship-to City', max_length=30)
    ship_to_contact = models.CharField(db_column='Ship-to Contact', max_length=50)
    order_date = models.CharField(db_column='Order Date')
    posting_date = models.CharField(db_column='Posting Date')
    shipment_date = models.CharField(db_column='Shipment Date')
    posting_description = models.CharField(db_column='Posting Description', max_length=50)
    payment_terms_code = models.CharField(db_column='Payment Terms Code', max_length=10)
    due_date = models.DateTimeField(db_column='Due Date')
    payment_discount_field = models.DecimalField(db_column='Payment Discount _', max_digits=38, decimal_places=20)
    pmt_discount_date = models.DateTimeField(db_column='Pmt_ Discount Date')
    shipment_method_code = models.CharField(db_column='Shipment Method Code', max_length=10)
    location_code = models.CharField(db_column='Location Code', max_length=10)
    shortcut_dimension_1_code = models.CharField(db_column='Shortcut Dimension 1 Code', max_length=20)
    shortcut_dimension_2_code = models.CharField(db_column='Shortcut Dimension 2 Code', max_length=20)
    customer_posting_group = models.CharField(db_column='Customer Posting Group', max_length=10)
    currency_code = models.CharField(db_column='Currency Code', max_length=10)
    currency_factor = models.DecimalField(db_column='Currency Factor', max_digits=38, decimal_places=20)
    customer_price_group = models.CharField(db_column='Customer Price Group', max_length=10)
    prices_including_vat = models.SmallIntegerField(db_column='Prices Including VAT')
    invoice_disc_code = models.CharField(db_column='Invoice Disc_ Code', max_length=20)
    customer_disc_group = models.CharField(db_column='Customer Disc_ Group', max_length=20)
    language_code = models.CharField(db_column='Language Code', max_length=10)
    salesperson_code = models.CharField(db_column='Salesperson Code', max_length=10)
    order_no_field = models.CharField(db_column='Order No_', max_length=20)
    no_printed = models.IntegerField(db_column='No_ Printed')
    on_hold = models.CharField(db_column='On Hold', max_length=3)
    applies_to_doc_type = models.IntegerField(db_column='Applies-to Doc_ Type')
    applies_to_doc_no_field = models.CharField(db_column='Applies-to Doc_ No_', max_length=20)
    bal_account_no_field = models.CharField(db_column='Bal_ Account No_', max_length=20)
    vat_registration_no_field = models.CharField(db_column='VAT Registration No_', max_length=20)
    reason_code = models.CharField(db_column='Reason Code', max_length=10)
    gen_bus_posting_group = models.CharField(db_column='Gen_ Bus_ Posting Group', max_length=10)
    eu_3_party_trade = models.SmallIntegerField(db_column='EU 3-Party Trade')
    transaction_type = models.CharField(db_column='Transaction Type', max_length=10)
    transport_method = models.CharField(db_column='Transport Method', max_length=10)
    vat_country_region_code = models.CharField(db_column='VAT Country_Region Code', max_length=10)
    sell_to_customer_name = models.CharField(db_column='Sell-to Customer Name', max_length=50)
    sell_to_customer_name_2 = models.CharField(db_column='Sell-to Customer Name 2', max_length=50)
    sell_to_address = models.CharField(db_column='Sell-to Address', max_length=50)
    sell_to_address_2 = models.CharField(db_column='Sell-to Address 2', max_length=50)
    sell_to_city = models.CharField(db_column='Sell-to City', max_length=30)
    sell_to_contact = models.CharField(db_column='Sell-to Contact', max_length=50)
    bill_to_post_code = models.CharField(db_column='Bill-to Post Code', max_length=20)
    bill_to_county = models.CharField(db_column='Bill-to County', max_length=30)
    bill_to_country_region_code = models.CharField(db_column='Bill-to Country_Region Code', max_length=10)
    sell_to_post_code = models.CharField(db_column='Sell-to Post Code', max_length=20)
    sell_to_county = models.CharField(db_column='Sell-to County', max_length=30)
    sell_to_country_region_code = models.CharField(db_column='Sell-to Country_Region Code', max_length=10)
    ship_to_post_code = models.CharField(db_column='Ship-to Post Code', max_length=20)
    ship_to_county = models.CharField(db_column='Ship-to County', max_length=30)
    ship_to_country_region_code = models.CharField(db_column='Ship-to Country_Region Code', max_length=10)
    bal_account_type = models.IntegerField(db_column='Bal_ Account Type')
    exit_point = models.CharField(db_column='Exit Point', max_length=10)
    correction = models.SmallIntegerField(db_column='Correction')
    document_date = models.DateTimeField(db_column='Document Date')
    external_document_no_field = models.CharField(db_column='External Document No_', max_length=35)
    area = models.CharField(db_column='Area', max_length=10)
    transaction_specification = models.CharField(db_column='Transaction Specification', max_length=10)
    payment_method_code = models.CharField(db_column='Payment Method Code', max_length=10)
    shipping_agent_code = models.CharField(db_column='Shipping Agent Code', max_length=10)
    package_tracking_no_field = models.CharField(db_column='Package Tracking No_', max_length=30)
    pre_assigned_no_series = models.CharField(db_column='Pre-Assigned No_ Series', max_length=10)
    no_series = models.CharField(db_column='No_ Series', max_length=10)
    order_no_series = models.CharField(db_column='Order No_ Series', max_length=10)
    pre_assigned_no_field = models.CharField(db_column='Pre-Assigned No_', max_length=20)
    user_id = models.CharField(db_column='User ID', max_length=50)
    source_code = models.CharField(db_column='Source Code', max_length=10)
    tax_area_code = models.CharField(db_column='Tax Area Code', max_length=20)
    tax_liable = models.SmallIntegerField(db_column='Tax Liable')
    vat_bus_posting_group = models.CharField(db_column='VAT Bus_ Posting Group', max_length=10)
    vat_base_discount_field = models.DecimalField(db_column='VAT Base Discount _', max_digits=38, decimal_places=20)
    campaign_no_field = models.CharField(db_column='Campaign No_', max_length=20)
    sell_to_contact_no_field = models.CharField(db_column='Sell-to Contact No_', max_length=20)
    bill_to_contact_no_field = models.CharField(db_column='Bill-to Contact No_', max_length=20)
    responsibility_center = models.CharField(db_column='Responsibility Center', max_length=10)
    allow_line_disc_field = models.SmallIntegerField(db_column='Allow Line Disc_')
    get_shipment_used = models.SmallIntegerField(db_column='Get Shipment Used')
    ship_to_ups_zone = models.CharField(db_column='Ship-to UPS Zone', max_length=2)
    tax_exemption_no_field = models.CharField(db_column='Tax Exemption No_', max_length=30)
    truck_no_field = models.CharField(db_column='Truck No_', max_length=30)
    commission_percent = models.DecimalField(db_column='Commission Percent', max_digits=38, decimal_places=20)
    packing_list_printed = models.SmallIntegerField(db_column='Packing List Printed')
    bol_printed = models.SmallIntegerField(db_column='BOL Printed')
    kaemark_truck_no_field = models.IntegerField(db_column='Kaemark Truck No_')
    store = models.CharField(db_column='Store', max_length=20)
    red_hot_rush = models.SmallIntegerField(db_column='Red Hot Rush')
    truck_stop = models.IntegerField(db_column='Truck Stop')
    vip_order = models.SmallIntegerField(db_column='VIP Order')
    ship_to_phone_number = models.CharField(db_column='Ship-to Phone Number', max_length=12)
    luxe_order = models.SmallIntegerField(db_column='Luxe Order')
    freight_charge_account = models.CharField(db_column='Freight Charge Account', max_length=20)
    oeperson_code = models.CharField(db_column='OEperson Code', max_length=10)
    return_status = models.IntegerField(db_column='Return Status')
    ship_to_email = models.CharField(db_column='Ship-to Email', max_length=50)
    prepayment_no_series = models.CharField(db_column='Prepayment No_ Series', max_length=10)
    prepayment_invoice = models.SmallIntegerField(db_column='Prepayment Invoice')
    prepayment_order_no_field = models.CharField(db_column='Prepayment Order No_', max_length=20)
    quote_no_field = models.CharField(db_column='Quote No_', max_length=20)
    credit_card_no_field = models.CharField(db_column='Credit Card No_', max_length=20)
    ste_transaction_id = models.CharField(db_column='STE Transaction ID', max_length=20)
    dimension_set_id = models.IntegerField(db_column='Dimension Set ID')
    electronic_document_sent = models.SmallIntegerField(db_column='Electronic Document Sent')
    original_document_xml = models.BinaryField(db_column='Original Document XML', blank=True, null=True)
    no_of_e_documents_sent = models.IntegerField(db_column='No_ of E-Documents Sent')
    original_string = models.BinaryField(db_column='Original String', blank=True, null=True)
    digital_stamp_sat = models.BinaryField(db_column='Digital Stamp SAT', blank=True, null=True)
    certificate_serial_no_field = models.CharField(db_column='Certificate Serial No_', max_length=250)
    signed_document_xml = models.BinaryField(db_column='Signed Document XML', blank=True, null=True)
    digital_stamp_pac = models.BinaryField(db_column='Digital Stamp PAC', blank=True, null=True)
    electronic_document_status = models.IntegerField(db_column='Electronic Document Status')
    date_time_stamped = models.CharField(db_column='Date_Time Stamped', max_length=50)
    date_time_sent = models.CharField(db_column='Date_Time Sent', max_length=50)
    date_time_canceled = models.CharField(db_column='Date_Time Canceled', max_length=50)
    error_code = models.CharField(db_column='Error Code', max_length=10)
    error_description = models.CharField(db_column='Error Description', max_length=250)
    pac_web_service_name = models.CharField(db_column='PAC Web Service Name', max_length=50)
    qr_code = models.BinaryField(db_column='QR Code', blank=True, null=True)
    fiscal_invoice_number_pac = models.CharField(db_column='Fiscal Invoice Number PAC', max_length=50)
    date_time_first_req_sent = models.CharField(db_column='Date_Time First Req_ Sent', max_length=50)
    replacement_order = models.SmallIntegerField(db_column='Replacement Order')
    replacement_reason = models.CharField(db_column='Replacement Reason', max_length=50)
    already_shipped = models.CharField(db_column='Already Shipped')
    ready_to_ship = models.CharField(db_column='Ready to Ship')
    payment_received = models.SmallIntegerField(db_column='Payment Received')
    payment_amount = models.CharField(db_column='Payment Amount', max_length=20)
    kaemark_shipping_person = models.CharField(db_column='Kaemark Shipping Person', max_length=30)
    email_code = models.CharField(db_column='Email Code', max_length=10)
    production_notes = models.CharField(db_column='Production Notes', max_length=250)
    production_color = models.CharField(db_column='Production Color', max_length=20)
    order_confirmation = models.SmallIntegerField(db_column='Order Confirmation')
    payment_reminder = models.SmallIntegerField(db_column='Payment Reminder')

    class Meta:
        managed = False
        db_table = 'Kaemark$Sales Invoice Header'
        unique_together = (('prepayment_order_no_field', 'prepayment_invoice', 'no_field'), ('bill_to_customer_no_field', 'no_field'), ('sell_to_customer_no_field', 'no_field'), ('pre_assigned_no_field', 'no_field'), ('order_no_field', 'no_field'),)


class KaemarkSalesInvoiceLine(models.Model):
    document_no_field = models.CharField(primary_key=True, db_column='Document No_', max_length=20)
    sell_to_customer_no_field = models.CharField(db_column='Sell-to Customer No_', max_length=20)
    invoice_line_type = models.IntegerField(db_column='Type')
    # this is the item Number
    no_field = models.CharField(db_column='No_', max_length=20)
    location_code = models.CharField(db_column='Location Code', max_length=10)
    posting_group = models.CharField(db_column='Posting Group', max_length=10)
    shipment_date = models.CharField(db_column='Shipment Date')
    # the descriptions
    description = models.CharField(db_column='Description', max_length=50)
    description_2 = models.CharField(db_column='Description 2', max_length=50)
    unit_of_measure = models.CharField(db_column='Unit of Measure', max_length=10)
    # the quantity
    original_quantity = models.DecimalField(db_column='Quantity', max_digits=38, decimal_places=20)
    unit_price = models.DecimalField(db_column='Unit Price', max_digits=38, decimal_places=20)
    unit_cost_lcy_field = models.DecimalField(db_column='Unit Cost (LCY)', max_digits=38, decimal_places=20)
    vat_field = models.DecimalField(db_column='VAT _', max_digits=38, decimal_places=20)
    line_discount_field = models.DecimalField(db_column='Line Discount _', max_digits=38, decimal_places=20)
    line_discount_amount = models.DecimalField(db_column='Line Discount Amount', max_digits=38, decimal_places=20)
    amount = models.DecimalField(db_column='Amount', max_digits=38, decimal_places=20)
    amount_including_vat = models.DecimalField(db_column='Amount Including VAT', max_digits=38, decimal_places=20)
    allow_invoice_disc_field = models.SmallIntegerField(db_column='Allow Invoice Disc_')
    gross_weight = models.DecimalField(db_column='Gross Weight', max_digits=38, decimal_places=20)
    net_weight = models.DecimalField(db_column='Net Weight', max_digits=38, decimal_places=20)
    units_per_parcel = models.DecimalField(db_column='Units per Parcel', max_digits=38, decimal_places=20)
    unit_volume = models.DecimalField(db_column='Unit Volume', max_digits=38, decimal_places=20)
    appl_to_item_entry = models.IntegerField(db_column='Appl_-to Item Entry')
    shortcut_dimension_1_code = models.CharField(db_column='Shortcut Dimension 1 Code', max_length=20)
    shortcut_dimension_2_code = models.CharField(db_column='Shortcut Dimension 2 Code', max_length=20)
    customer_price_group = models.CharField(db_column='Customer Price Group', max_length=10)
    job_no_field = models.CharField(db_column='Job No_', max_length=20)
    work_type_code = models.CharField(db_column='Work Type Code', max_length=10)
    shipment_no_field = models.CharField(db_column='Shipment No_', max_length=20)
    shipment_line_no_field = models.IntegerField(db_column='Shipment Line No_')
    bill_to_customer_no_field = models.CharField(db_column='Bill-to Customer No_', max_length=20)
    inv_discount_amount = models.DecimalField(db_column='Inv_ Discount Amount', max_digits=38, decimal_places=20)
    drop_shipment = models.SmallIntegerField(db_column='Drop Shipment')
    gen_bus_posting_group = models.CharField(db_column='Gen_ Bus_ Posting Group', max_length=10)
    gen_prod_posting_group = models.CharField(db_column='Gen_ Prod_ Posting Group', max_length=10)
    vat_calculation_type = models.IntegerField(db_column='VAT Calculation Type')
    transaction_type = models.CharField(db_column='Transaction Type', max_length=10)
    transport_method = models.CharField(db_column='Transport Method', max_length=10)
    attached_to_line_no_field = models.IntegerField(db_column='Attached to Line No_')
    exit_point = models.CharField(db_column='Exit Point', max_length=10)
    area = models.CharField(db_column='Area', max_length=10)
    transaction_specification = models.CharField(db_column='Transaction Specification', max_length=10)
    tax_area_code = models.CharField(db_column='Tax Area Code', max_length=20)
    tax_liable = models.SmallIntegerField(db_column='Tax Liable')
    tax_group_code = models.CharField(db_column='Tax Group Code', max_length=10)
    vat_bus_posting_group = models.CharField(db_column='VAT Bus_ Posting Group', max_length=10)
    vat_prod_posting_group = models.CharField(db_column='VAT Prod_ Posting Group', max_length=10)
    blanket_order_no_field = models.CharField(db_column='Blanket Order No_', max_length=20)
    blanket_order_line_no_field = models.IntegerField(db_column='Blanket Order Line No_')
    vat_base_amount = models.DecimalField(db_column='VAT Base Amount', max_digits=38, decimal_places=20)
    unit_cost = models.DecimalField(db_column='Unit Cost', max_digits=38, decimal_places=20)
    system_created_entry = models.SmallIntegerField(db_column='System-Created Entry')
    line_amount = models.DecimalField(db_column='Line Amount', max_digits=38, decimal_places=20)
    vat_difference = models.DecimalField(db_column='VAT Difference', max_digits=38, decimal_places=20)
    vat_identifier = models.CharField(db_column='VAT Identifier', max_length=10)
    ic_partner_ref_type = models.IntegerField(db_column='IC Partner Ref_ Type')
    ic_partner_reference = models.CharField(db_column='IC Partner Reference', max_length=20)
    variant_code = models.CharField(db_column='Variant Code', max_length=10)
    bin_code = models.CharField(db_column='Bin Code', max_length=20)
    qty_per_unit_of_measure = models.DecimalField(db_column='Qty_ per Unit of Measure', max_digits=38, decimal_places=20)
    unit_of_measure_code = models.CharField(db_column='Unit of Measure Code', max_length=10)
    quantity_base_field = models.DecimalField(db_column='Quantity (Base)', max_digits=38, decimal_places=20)
    fa_posting_date = models.CharField(db_column='FA Posting Date')
    depreciation_book_code = models.CharField(db_column='Depreciation Book Code', max_length=10)
    depr_until_fa_posting_date = models.SmallIntegerField(db_column='Depr_ until FA Posting Date')
    duplicate_in_depreciation_book = models.CharField(db_column='Duplicate in Depreciation Book', max_length=10)
    use_duplication_list = models.SmallIntegerField(db_column='Use Duplication List')
    responsibility_center = models.CharField(db_column='Responsibility Center', max_length=10)
    cross_reference_no_field = models.CharField(db_column='Cross-Reference No_', max_length=20)
    unit_of_measure_cross_ref_field = models.CharField(db_column='Unit of Measure (Cross Ref_)', max_length=10)
    cross_reference_type = models.IntegerField(db_column='Cross-Reference Type')
    cross_reference_type_no_field = models.CharField(db_column='Cross-Reference Type No_', max_length=30)
    item_category_code = models.CharField(db_column='Item Category Code', max_length=10)
    nonstock = models.SmallIntegerField(db_column='Nonstock')
    purchasing_code = models.CharField(db_column='Purchasing Code', max_length=10)
    product_group_code = models.CharField(db_column='Product Group Code', max_length=10)
    appl_from_item_entry = models.IntegerField(db_column='Appl_-from Item Entry')
    return_reason_code = models.CharField(db_column='Return Reason Code', max_length=10)
    allow_line_disc_field = models.SmallIntegerField(db_column='Allow Line Disc_')
    customer_disc_group = models.CharField(db_column='Customer Disc_ Group', max_length=20)
    package_tracking_no_field = models.CharField(db_column='Package Tracking No_', max_length=30)
    kaemark_work_order = models.CharField(db_column='Kaemark Work Order', max_length=30)
    qty_pulled = models.DecimalField(db_column='Qty Pulled', max_digits=38, decimal_places=20)
    shipping_weight = models.DecimalField(db_column='Shipping Weight', max_digits=38, decimal_places=20)
    shipping_classification = models.CharField(db_column='Shipping Classification', max_length=30)
    shipping_cartons = models.IntegerField(db_column='Shipping # Cartons')
    shipping_loaded = models.SmallIntegerField(db_column='Shipping Loaded')
    return_no_field = models.IntegerField(db_column='Return No_')
    qc_person = models.CharField(db_column='QC Person', max_length=30)
    prepayment_line = models.SmallIntegerField(db_column='Prepayment Line')
    ic_partner_code = models.CharField(db_column='IC Partner Code', max_length=20)
    posting_date = models.CharField(db_column='Posting Date')
    job_task_no_field = models.CharField(db_column='Job Task No_', max_length=20)
    job_contract_entry_no_field = models.IntegerField(db_column='Job Contract Entry No_')
    vat_clause_code = models.CharField(db_column='VAT Clause Code', max_length=10)
    dimension_set_id = models.IntegerField(db_column='Dimension Set ID')
    corrective_action = models.CharField(db_column='Corrective Action', max_length=250)
    root_cause = models.CharField(db_column='Root Cause', max_length=250)
    external_document_no = models.CharField(db_column='External Document No', max_length=25)

    class Meta:
        managed = False
        db_table = 'Kaemark$Sales Invoice Line'
        unique_together = (('document_no_field', 'line_no_field'), ('bill_to_customer_no_field', 'document_no_field', 'line_no_field'), ('shipment_no_field', 'shipment_line_no_field', 'document_no_field', 'line_no_field'), ('job_contract_entry_no_field', 'document_no_field', 'line_no_field'), ('sell_to_customer_no_field', 'document_no_field', 'line_no_field'), ('blanket_order_no_field', 'blanket_order_line_no_field', 'document_no_field', 'line_no_field'),)
