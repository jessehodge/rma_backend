from rest_framework.generics import ListAPIView
from rest_framework.viewsets import ModelViewSet
from serializers import SalesInvoiceHeaderSerializer, SalesInvoiceLineSerializer
from models import KaemarkSalesInvoiceHeader, KaemarkSalesInvoiceLine

# Create your views here.


class SalesInvoiceHeaderViewSet(ModelViewSet):
    serializer_class = SalesInvoiceHeaderSerializer
    queryset = KaemarkSalesInvoiceHeader.objects.all()


class SalesInvoiceLineViewSet(ListAPIView):
    serializer_class = SalesInvoiceLineSerializer

    def get_queryset(self, *args, **kwargs):
        invoice_number = self.kwargs['no_field']
        invoices = KaemarkSalesInvoiceLine.objects.filter(document_no_field=invoice_number)
        return invoices

class SalesInvoiceHeaderOrderNumberViewSet(ListAPIView):
    serializer_class = SalesInvoiceHeaderSerializer

    def get_queryset(self, *args, **kwargs):
        order_number = self.kwargs['order_no']
        order = KaemarkSalesInvoiceHeader.objects.filter(order_no_field=order_number)
        return order
