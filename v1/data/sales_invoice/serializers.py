from rest_framework.serializers import ModelSerializer
from models import KaemarkSalesInvoiceHeader, KaemarkSalesInvoiceLine


class SalesInvoiceLineSerializer(ModelSerializer):

    class Meta:
        model = KaemarkSalesInvoiceLine
        fields = '__all__'


class SalesInvoiceHeaderSerializer(ModelSerializer):
    items = SalesInvoiceLineSerializer(many=True, read_only=True)

    class Meta:
        model = KaemarkSalesInvoiceHeader
        fields = '__all__'
