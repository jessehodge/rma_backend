from rest_framework.serializers import ModelSerializer
from models import PalletHeader, PalletLine, SalesHeader, SalesLine
from rest_framework_bulk import BulkSerializerMixin, BulkListSerializer

class PalletLineSerializer(BulkSerializerMixin, ModelSerializer):

    class Meta(object):
        model = PalletLine
        list_serializer_class = BulkListSerializer
        update_lookup_field = 'id'
        fields = '__all__'


class PalletHeaderSerializer(ModelSerializer):

    class Meta(object):
        model = PalletHeader
        fields = '__all__'

class SalesHeaderSerializer(ModelSerializer):

    class Meta(object):
        model = SalesHeader
        fields = [
            'kaemark_truck_no_field'
        ]
        read_only = ['no_field']

class SalesLineSerializer(ModelSerializer):

    class Meta(object):
        model = SalesLine
        fields = [
            'box_info_key',
            'no_field',
            'description',
            'description_2',
        ]
