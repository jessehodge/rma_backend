from rest_framework.viewsets import ModelViewSet
from rest_framework_bulk import BulkModelViewSet
from rest_framework.generics import ListAPIView
from serializers import *
from models import *
# Create your views here.

class PalletHeaderViewSet(ModelViewSet):
    serializer_class = PalletHeaderSerializer
    queryset = PalletHeader.objects.all()

class PalletLineViewSet(BulkModelViewSet):
    serializer_class = PalletLineSerializer
    queryset = PalletLine.objects.all()


class PalletLineFilterViewSet(ListAPIView):
    serializer_class = PalletLineSerializer

    def get_queryset(self):
        pallet_number = self.kwargs['pallet_number']
        return PalletLine.objects.filter(pallet_number=pallet_number)

class PalletLineBoxInfoKeyFilterViewSet(ListAPIView):
    serializer_class = PalletLineSerializer

    def get_queryset(self):
        box_info_key = self.kwargs['box_info_key']
        return PalletLine.objects.filter(box_info_key=box_info_key)


class PalletLineBoxFilterViewSet(ListAPIView):
    serializer_class = PalletLineSerializer

    def get_queryset(self):
        box_no = self.kwargs['box_no']
        box_info_key = self.kwargs['box_info_key']
        return PalletLine.objects.filter(box_no=box_no, box_info_key=box_info_key)

class SalesHeaderViewSet(ListAPIView):
    serializer_class = SalesHeaderSerializer

    def get_queryset(self):
        no_field = self.kwargs['no_field']
        return SalesHeader.objects.filter(no_field=no_field)


class SalesLineViewSet(ListAPIView):
    serializer_class = SalesLineSerializer

    def get_queryset(self):
        box_info_key = self.kwargs['box_info_key']
        return SalesLine.objects.filter(box_info_key=box_info_key)


class SalesLineLoadingLocationViewSet(ListAPIView):
    serializer_class = SalesLineSerializer

    def get_queryset(self):
        loading_location = self.kwargs['loading_location']
        return LoadingLocation.objects.filter(loading_location=loading_location)
