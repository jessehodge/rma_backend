from __future__ import unicode_literals

from django.apps import AppConfig


class SalesLineItemConfig(AppConfig):
    name = 'sales_line_item'
