from rest_framework_bulk.routes import BulkRouter
from django.conf.urls import url
from views import *

router = BulkRouter()
router.register(r'pallet_header', PalletHeaderViewSet)
router.register(r'pallet_line', PalletLineViewSet)

urlpatterns = [
    url('^pallet_line/pallet/(?P<pallet_number>\d+)/$', PalletLineFilterViewSet.as_view()),
    url('^pallet_line/shipping/box/(?P<box_info_key>.+)/(?P<box_no>.+)/$', PalletLineBoxFilterViewSet.as_view()),
    url('^pallet_line/box_info/(?P<box_info_key>.+)/$', PalletLineBoxInfoKeyFilterViewSet.as_view()),
    url('^sales_header/(?P<no_field>.+)/$', SalesHeaderViewSet.as_view()),
    url('^sales_line/(?P<box_info_key>.+)/$', SalesLineViewSet.as_view()),
    url('^pallet_line/loading_location/(?P<loading_location>.+)/$', SalesLineLoadingLocationViewSet.as_view()),
]

urlpatterns += router.urls
