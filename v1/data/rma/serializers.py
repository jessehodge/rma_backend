from rest_framework.serializers import ModelSerializer
from v1.data.rma.models import RmaHeader, RmaLines, RmaMessages
from rest_framework_bulk import BulkSerializerMixin, ListBulkCreateUpdateDestroyAPIView, BulkListSerializer
from django.core.mail import send_mail


class RmaLineSerializer(BulkSerializerMixin, ModelSerializer):
    class Meta(object):
        model = RmaLines
        fields = [
            'id',
            'rma_header_id',
            'description',
            'description_2',
            'original_quantity',
            'returned_quantity',
            'expected_return_quantity',
            'shipping_weight',
            'shipping_classification',
            'rts_qty',
            'rtm_qty',
            'scrap_qty',
            'invoice_number',
            'return_completed',
            'amount_credited'
        ]
        read_only_fields = ['id']
        list_serializer_class = BulkListSerializer
        update_lookup_field = 'id'


class RmaMessageSerializer(ModelSerializer):
    class Meta(object):
        model = RmaMessages
        fields = [
            'id',
            'rma_header_id',
            'date_posted',
            'department',
            'sender',
            'message'
        ]
        read_only_fields = ['id']



class RmaHeaderSerializer(ModelSerializer):
    lines = RmaLineSerializer(many=True, read_only=True)
    messages = RmaMessageSerializer(many=True, read_only=True)
    class Meta(object):
        model = RmaHeader
        fields = [
            'id',
            'request_date',
            'call_tag',
            'salesperson_code',
            'credit_memo_number',
            'no_field',
            'sell_to_customer_name',
            'ship_to_address',
            'ship_to_address_2',
            'ship_to_city',
            'ship_to_county',
            'ship_to_post_code',
            'ship_to_email',
            'ship_to_phone',
            'pick_up_day',
            'pick_up_hours',
            'pick_up_residential',
            'return_reason',
            'credit_terms',
            'return_address',
            'cm_restocking_charge',
            'cm_deduct_outgoing_freight',
            'cm_deduct_incoming_freight',
            'credit_notes',
            'credit_memo_required',
            'received_complete',
            'accounting_complete',
            'pick_up_notes',
            'lines',
            'messages',
            'status',
            'call_tag_number'
        ]
        read_only_fields = ['id', 'request_date']
