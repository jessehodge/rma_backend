from rest_framework_bulk.routes import BulkRouter
from django.conf.urls import url
from v1.data.rma.views import (
    RmaHeaderViewSet,
    RmaLineViewSet,
    ReceivingRmaLines,
    AccountingRmaHeaders,
    AccountingCompleteRmaHeaders,
    RmaOeLines,
    ReceivingRmaHeaders,
    OeRmaDetail,
    RmaMessagesView,
    RmaMessagesViewSet,
    ReceivingCompleteRmaHeaders
)

router = BulkRouter()
router.register(r'rma_header', RmaHeaderViewSet)
router.register(r'rma_line', RmaLineViewSet)
router.register(r'messages', RmaMessagesViewSet)

urlpatterns = [
    url('^rma_line/receiving/(?P<rma_header_id>.+)/$', ReceivingRmaLines.as_view()),
    url('^rma_line/oe/(?P<invoice_number>.+)/$', RmaOeLines.as_view()),
    url('^rma_header/detail/(?P<rma_header_id>.+)/$', OeRmaDetail.as_view()),
    url('^rma_header/accounting/$', AccountingRmaHeaders.as_view()),
    url('^rma_header/accounting/complete/$', AccountingCompleteRmaHeaders.as_view()),
    url('^rma_header/receiving/$', ReceivingRmaHeaders.as_view()),
    url('^rma_header/receiving/complete/$', ReceivingCompleteRmaHeaders.as_view()),
    url('^messages/detail/(?P<rma_header_id>.+)/$', RmaMessagesView.as_view())
]

urlpatterns += router.urls
