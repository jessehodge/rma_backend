from rest_framework_bulk import BulkModelViewSet
from rest_framework import generics, viewsets, status
from rest_framework.response import Response
from v1.data.rma.serializers import RmaHeaderSerializer, RmaLineSerializer, RmaMessageSerializer
from v1.data.rma.models import RmaHeader, RmaLines, RmaMessages
from django.core.mail import send_mail, EmailMessage
from django.template import Context, Template
from django.http import HttpResponse
# Create your views here.


class RmaHeaderViewSet(viewsets.ModelViewSet):
    serializer_class = RmaHeaderSerializer
    queryset = RmaHeader.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        '''
        if request.method == "POST":
            header_id = serializer.data['id']
            call_tag = request.data['call_tag']
            t = Template(
                "Request #: {{return_id}}<br>" +
                "Please have a call tag issued for the following return.<br><br>" +
                "<strong>Product is being returned because:</strong> {{return_reason}}<br>" +
                "<strong>Call tag status is:</strong> {{call_tag}}<br>" +
                "<strong>Please pick up the product from:</strong> {{customer_name}}<br>" +
                "<p><strong>Located at: </strong><br>" +
                "{{pick_up_address}} " + "{{pick_up_address_2}}<br>" +
                "{{city}}, " + "{{state}} {{zip_code}}</p><br>" +
                "<strong>Customer Phone Number:</strong> {{phone_number}}<br>" +
                "<strong>Customer Email Address:</strong> {{email_address}}<br>" +
                "<strong>Day ready for pickup:</strong> {{pick_up_day}}<br>" +
                "<strong>Time ready to be picked up:</strong> {{pick_up_hours}}<br>" +
                "<strong>Company that it's going to be returned to:</strong> {{return_address}}"
            )
            c = Context({
                'return_id': serializer.data['id'],
                'call_tag': request.data['call_tag'],
                'return_reason': request.data['return_reason'],
                'customer_name': request.data['sell_to_customer_name'],
                'pick_up_address': request.data['ship_to_address'],
                'pick_up_address_2': request.data['ship_to_address_2'],
                'city': request.data['ship_to_city'],
                'state': request.data['ship_to_county'],
                'zip_code': request.data['ship_to_post_code'],
                'pick_up_day': request.data['pick_up_day'],
                'pick_up_hours': request.data['pick_up_hours'],
                'return_address': request.data['return_address']
            })
            html_content = t.render(c)
            subject = 'A Return is ready for you to view. The type of return is ' + call_tag
            ltl_emails = [
                'kaemark@tpslogistics.com',
                'returns@kaemark.com'
                ]
            if call_tag == "LTL":
                email = EmailMessage('Call Tag', html_content, "jesse@kaemark.com", ltl_emails)
                email.content_subtype = "html"
                email.send()
            elif call_tag == "FEG":
                email = EmailMessage('FEG', html_content, "jesse@kaemark.com", [
                    'nashanda@kaemark.com',
                    'returns@kaemark.com'
                    ])
                email.content_subtype = "html"
                email.send()
            elif call_tag == "No call tag - Customer is sending back themselves":
                email = EmailMessage(
                    'No call tag - Customer is sending back themselves',
                    html_content,
                    "jesse@kaemark.com",
                    ['returns@kaemark.com']
                )
                email.content_subtype = "html"
                email.send()
            elif call_tag == "No call tag - Need to provide a credit memo only":
                email = EmailMessage(
                    'No call tag - Need to provide a credit memo only',
                    html_content,
                    "jesse@kaemark.com",
                    ['racheal@kaemark.com']
                )
                email.content_subtype = "html"
                email.send()
            else:
                print "This didn't work!"
                '''
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class RmaLineViewSet(BulkModelViewSet):
    queryset = RmaLines.objects.all()
    serializer_class = RmaLineSerializer


class RmaMessagesViewSet(BulkModelViewSet):
    serializer_class = RmaMessageSerializer
    queryset = RmaMessages.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        if request.method == "POST":
            to_dept = request.data['department']
            whom_its_from = request.data['sender']
            header_id = request.data['rma_header_id']
            message = 'You have a message from ' + whom_its_from + '. Please click on the link to view your message '
            header = str(header_id)
            order_entry_url = 'http://server2/oe/rma/detail/' + header
            accounting_url = 'http://server2/accounting/rma/detail/' +  header
            receiving_url = 'http://server2/receiving/rma/detail/' +  header

            if to_dept == 'Order Entry':
                send_mail(
                    'You have a new message!',
                    message + order_entry_url,
                    'jesse@kaemark.com',
                    ['megan@kaemark.com']
                )
            elif to_dept == 'Accounting':
                send_mail(
                    'You have a new message!',
                    message + accounting_url,
                    'jesse@kaemark.com',
                    ['racheal@kaemark.com']
                )
            elif to_dept == 'Receiving':
                send_mail(
                    'You have a new message!',
                    message + receiving_url,
                    'jesse@kaemark.com',
                    ['brent@kaemark.com']
                )
            else:
                print "It didn't work!"

        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class RmaMessagesView(generics.ListAPIView):
    serializer_class = RmaMessageSerializer

    def get_queryset(self):
        header_id = self.kwargs['rma_header_id']
        return RmaMessages.objects.filter(rma_header_id=header_id)

class AccountingRmaHeaders(generics.ListAPIView):
    serializer_class = RmaHeaderSerializer

    def get_queryset(self):
        return RmaHeader.objects.filter(credit_memo_required=True, status="Completed").exclude(accounting_complete=True)


class AccountingCompleteRmaHeaders(generics.ListAPIView):
    serializer_class = RmaHeaderSerializer

    def get_queryset(self):
        return RmaHeader.objects.filter(credit_memo_required=True, status="Completed", accounting_complete=True)


class RmaOeLines(generics.ListAPIView):
    serializer_class = RmaLineSerializer

    def get_queryset(self):
        invoice = self.kwargs['invoice_number']
        return RmaLines.objects.filter(invoice_number=invoice)


class OeRmaDetail(generics.ListAPIView):
    serializer_class = RmaLineSerializer

    def get_queryset(self):
        header_number = self.kwargs['rma_header_id']
        return RmaLines.objects.filter(rma_header_id=header_number)


class ReceivingRmaHeaders(generics.ListAPIView):
    serializer_class = RmaHeaderSerializer

    def get_queryset(self):
        return RmaHeader.objects.exclude(status="Completed")


class ReceivingCompleteRmaHeaders(generics.ListAPIView):
    serializer_class = RmaHeaderSerializer

    def get_queryset(self):
        return RmaHeader.objects.filter(status="Completed")


class ReceivingRmaLines(generics.ListAPIView):
    serializer_class = RmaLineSerializer

    def get_queryset(self):
        header_id = self.kwargs['rma_header_id']
        return RmaLines.objects.filter(rma_header_id=header_id)
