from __future__ import unicode_literals

from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save

# Create your models here.


class KaemarkSalespersonPurchaser(models.Model):
    code = models.CharField(db_column='Code', primary_key=True, max_length=10)
    name = models.CharField(db_column='Name', max_length=50)
    commission_field = models.DecimalField(
        db_column='Commission _',
        max_digits=38,
        decimal_places=20)
    global_dimension_1_code = models.CharField(db_column='Global Dimension 1 Code', max_length=20)
    global_dimension_2_code = models.CharField(db_column='Global Dimension 2 Code', max_length=20)
    e_mail = models.CharField(db_column='E-Mail', max_length=80)
    phone_no_field = models.CharField(db_column='Phone No_', max_length=200)
    job_title = models.CharField(db_column='Job Title', max_length=200)
    search_e_mail = models.CharField(db_column='Search E-Mail', max_length=80)
    e_mail_2 = models.CharField(db_column='E-Mail 2', max_length=80)

    class Meta:
        managed = False
        db_table = 'Kaemark$Salesperson_Purchaser'
        unique_together = (('search_e_mail', 'code'),)

    def __unicode__(self):
        return self.name


class RmaHeader(models.Model):
    PICK_UP_HOURS_CHOICES = (
        ('Anytime', 'Anytime'),
        ('Morning', 'Morning'),
        ('Afternoon', 'Afternoon'),
    )

    RETURN_ADDRESS_CHOICES = (
        ('Kaemark, Inc., 1338 CR 208, Giddings, TX 78942',
         'Kaemark, Inc., 1338 CR 208, Giddings, TX 78942'),
        ('Highland Machine, 700 5th Street, Highland, IL 62249',
         'Highland Machine, 700 5th Street, Highland, IL 62249'),
        ('Smart Step, 1000 Independence Dr #4, Sullivan, MO 63080',
         'Smart Step, 1000 Independence Dr #4, Sullivan, MO 63080'),
    )

    RETURN_STATUS = (
        ('New', 'New'),
        ('Waiting for more', 'Waiting for more'),
        ('Completed', 'Completed'),
    )

    salesperson_code = models.ForeignKey(
        'KaemarkSalespersonPurchaser',
        models.DO_NOTHING,
        db_column='request_user_code',
        blank=True,
        null=True)
    credit_memo_number = models.ForeignKey(
        'KaemarkSalesCrMemoHeader',
        models.DO_NOTHING,
        db_column='credit_memo_number',
        blank=True,
        null=True)
    no_field = models.ForeignKey(
        'KaemarkSalesInvoiceHeader',
        models.DO_NOTHING,
        db_column='invoice_number',
        blank=True, null=True)
    request_date = models.DateField(auto_now=True)
    call_tag = models.TextField(blank=True, null=True)
    credit_memo_required = models.NullBooleanField()
    # Customer Information
    sell_to_customer_name = models.TextField(blank=True, null=True)
    ship_to_address = models.TextField(blank=True, null=True)
    ship_to_address_2 = models.TextField(blank=True, null=True)
    ship_to_city = models.TextField(blank=True, null=True)
    ship_to_county = models.TextField(blank=True, null=True)
    ship_to_post_code = models.TextField(blank=True, null=True)
    ship_to_email = models.TextField(blank=True, null=True)
    ship_to_phone = models.TextField(blank=True, null=True)
    pick_up_day = models.TextField(blank=True, null=True)
    pick_up_hours = models.TextField(choices=PICK_UP_HOURS_CHOICES)
    return_address = models.TextField(choices=RETURN_ADDRESS_CHOICES)
    pick_up_notes = models.TextField(blank=True, null=True)
    pick_up_residential = models.NullBooleanField()
    # Return Information
    return_reason = models.TextField(blank=True, null=True)
    credit_terms = models.NullBooleanField()
    cm_restocking_charge = models.TextField(blank=True, null=True)
    cm_deduct_outgoing_freight = models.NullBooleanField(default=False)
    cm_deduct_incoming_freight = models.NullBooleanField(default=False)
    received_complete = models.NullBooleanField()
    accounting_complete = models.NullBooleanField()
    call_tag_number = models.TextField(blank=True, null=True)
    credit_notes = models.TextField(blank=True, null=True)
    status = models.CharField(
        choices=RETURN_STATUS,
        default="New",
        max_length=200,
        blank=True,
        null=True)

    class Meta:
        managed = False
        db_table = 'Kaemark$RMA Header'


class RmaMessages(models.Model):
    rma_header_id = models.ForeignKey(RmaHeader, unique=False, null=True)
    date_posted = models.DateField(auto_now_add=True)
    department = models.CharField(max_length=200, blank=True, null=True)
    sender = models.CharField(max_length=200, blank=True, null=True)
    message = models.CharField(max_length=255, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Kaemark$RMA Messages'

    def __unicode__(self):
        return self.rma_header_id


class RmaLines(models.Model):
    rma_header_id = models.ForeignKey(RmaHeader, unique=False, null=True)
    description = models.CharField(max_length=200, blank=True, null=True)
    description_2 = models.CharField(blank=True, max_length=200, null=True)
    shipping_weight = models.DecimalField(max_digits=38, decimal_places=20, blank=True, null=True)
    shipping_classification = models.CharField(max_length=200, blank=True, null=True)
    original_quantity = models.CharField(max_length=200, blank=True, null=True)
    expected_return_quantity = models.CharField(max_length=200, blank=True, null=True)
    returned_quantity = models.CharField(max_length=200, blank=True, null=True)
    credit_quantity = models.CharField(max_length=200, blank=True, null=True)
    rtm_qty = models.CharField(max_length=200, blank=True, null=True)
    rts_qty = models.CharField(max_length=200, blank=True, null=True)
    scrap_qty = models.CharField(max_length=200, blank=True, null=True)
    returned_condition = models.CharField(max_length=200, blank=True, null=True)
    invoice_number = models.CharField(max_length=200, blank=True, null=True)
    return_completed = models.NullBooleanField()
    amount_credited = models.CharField(max_length=200, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'Kaemark$RMA Lines'

    def __unicode__(self):
        return self.invoice_number
