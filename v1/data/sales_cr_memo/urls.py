from __future__ import unicode_literals
from rest_framework.routers import DefaultRouter
from django.conf.urls import url
from views import SalesCrMemoHeaderViewSet, SalesCrMemoLineViewSet

router = DefaultRouter()
router.register(r'sales_cr_memo_header', SalesCrMemoHeaderViewSet)
router.register(r'sales_cr_memo_line', SalesCrMemoLineViewSet)

urlpatterns = router.urls
