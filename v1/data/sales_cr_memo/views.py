from __future__ import unicode_literals
from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from serializers import SalesCrMemoHeaderSerializer, SalesCrMemoLineSerializer
from models import KaemarkSalesCrMemoHeader, KaemarkSalesCrMemoLine

# Create your views here.


class SalesCrMemoHeaderViewSet(ModelViewSet):
    serializer_class = SalesCrMemoHeaderSerializer
    queryset = KaemarkSalesCrMemoHeader.objects.all()


class SalesCrMemoLineViewSet(ModelViewSet):
    serializer_class = SalesCrMemoLineSerializer
    queryset = KaemarkSalesCrMemoLine.objects.all()
