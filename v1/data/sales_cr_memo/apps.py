from __future__ import unicode_literals

from django.apps import AppConfig


class SalesCrMemoConfig(AppConfig):
    name = 'sales_cr_memo'
