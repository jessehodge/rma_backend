from __future__ import unicode_literals

from django.apps import AppConfig


class BomComponentConfig(AppConfig):
    name = 'bom_component'
