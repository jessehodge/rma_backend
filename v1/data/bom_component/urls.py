from rest_framework.routers import DefaultRouter
from django.conf.urls import url
from views import BomComponentViewSet

router = DefaultRouter()
router.register(r'bom_component', BomComponentViewSet, base_name='bom_component')

urlpatterns = router.urls
