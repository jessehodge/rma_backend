from django.shortcuts import render
from rest_framework.viewsets import ModelViewSet
from serializers import BomComponentSerializer
from models import KaemarkBomComponent

# Create your views here.


class BomComponentViewSet(ModelViewSet):
    serializer_class = BomComponentSerializer
    queryset = KaemarkBomComponent.objects.all()
