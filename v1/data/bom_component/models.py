from __future__ import unicode_literals

from django.db import models

# Create your models here.

class KaemarkBomComponent(models.Model):
    timestamp = models.TextField()  # This field type is a guess.
    parent_item_no_field = models.CharField(db_column='Parent Item No_', max_length=20)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    line_no_field = models.IntegerField(db_column='Line No_')  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    bom_type = models.IntegerField(db_column='Type')  # Field name made lowercase.
    no_field = models.CharField(db_column='No_', max_length=20)  # Field name made lowercase. Field renamed because it ended with '_'.
    description = models.CharField(db_column='Description', max_length=50)  # Field name made lowercase.
    unit_of_measure_code = models.CharField(db_column='Unit of Measure Code', max_length=10)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    quantity_per = models.DecimalField(db_column='Quantity per', max_digits=38, decimal_places=20)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    position = models.CharField(db_column='Position', max_length=10)  # Field name made lowercase.
    position_2 = models.CharField(db_column='Position 2', max_length=10)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    position_3 = models.CharField(db_column='Position 3', max_length=10)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    machine_no_field = models.CharField(db_column='Machine No_', max_length=10)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    lead_time_offset = models.CharField(db_column='Lead-Time Offset', max_length=32)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    resource_usage_type = models.IntegerField(db_column='Resource Usage Type')  # Field name made lowercase. Field renamed to remove unsuitable characters.
    variant_code = models.CharField(db_column='Variant Code', max_length=10)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    installed_in_line_no_field = models.IntegerField(db_column='Installed in Line No_')  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    installed_in_item_no_field = models.CharField(db_column='Installed in Item No_', max_length=20)  # Field name made lowercase. Field renamed to remove unsuitable characters. Field renamed because it ended with '_'.
    class Meta:
        managed = False
        db_table = 'Kaemark$BOM Component'
        unique_together = (('parent_item_no_field', 'line_no_field'), ('type', 'no_field', 'parent_item_no_field', 'line_no_field'),)
