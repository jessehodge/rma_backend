from rest_framework import serializers
from models import KaemarkBomComponent

class BomComponentSerializer(serializers.ModelSerializer):

    class Meta:
        model = KaemarkBomComponent
        fields = [
            'timestamp',
            'parent_item_no_field',
            'line_no_field',
            'bom_type',
            'no_field',
            'description',
            'unit_of_measure_code',
            'quantity_per',
            'position',
            'position_2',
            'position_3',
            'machine_no_field',
            'lead_time_offset',
            'resource_usage_type',
            'variant_code',
            'installed_in_line_no_field',
            'installed_in_item_no_field',
    ]
